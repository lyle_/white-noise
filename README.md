# White Noise

You are an NPN transistor who finds yourself plucked from the bag, suddenly hopeful about a productive new life spent switching important signals for a good cause.
Things don't look good right off the bat, though; the wire cutters come out and your collector leg is amputated suddenly and without ceremony.
That's not how this normally goes.
That's not right.

You're shoved into an unfamiliar circuit...
Surely there must be some mistake?
Instead of the normal base-emitter voltage you exist to handle, your remaining limbs are hit with a reverse-biased charge.
It's not right... but you can take it.
For a little while.

After that, you scream.

## Mechanisms

Overall scream strength is controlled by the gag knob.

Screams can be coerced into low or high frequency ranges by the `torture` knob.

## References

Initially inspired by the circuit in Thomas Henry's *The Electronic Drum Cookbook*.
It's awesome and simple.
Build it, bathe in the noise, and become one with the noise.

Then consider fucking it all up by making it way more complicated.
Further evolutionary genes come from the [MFOS circuit](http://musicfromouterspace.com/analogsynth_new/EXPERIMENTERBOARD/page7.html).
It's considerably more complicated, but it's from Outer Space so I guess it would be.
It's louder, so it's worth it I guess.

Filter section derived from [synthnerd](https://synthnerd.wordpress.com/2020/03/27/synth-diy-a-white-noise-generator-part-2/).
Because it needed another knob.
